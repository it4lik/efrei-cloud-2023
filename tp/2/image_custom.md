# Partie 2 : Créer et utiliser une image custom

- [Partie 2 : Créer et utiliser une image custom](#partie-2--créer-et-utiliser-une-image-custom)
- [I. Création image custom](#i-création-image-custom)
  - [1. Créer une VM](#1-créer-une-vm)
  - [2. Configurer la VM](#2-configurer-la-vm)
  - [3. Créer une image custom](#3-créer-une-image-custom)
  - [3. Créer une nouvelle VM](#3-créer-une-nouvelle-vm)
- [II. Tester : spécialiser les VMs au lancement](#ii-tester--spécialiser-les-vms-au-lancement)
- [Bilan et cleanup](#bilan-et-cleanup)

On va utiliser des fonctionnalités de Azure pour créer une image de base qui pourra ensuite être utilisée pour créer des VMs.

C'est par exemple idéal dans un cadre pro pour proposer une image de VM de base qui contient les standards de sécurité de la boîte par exemple.

On va donc :

- créer une machine dans Azure
  - en utilisant une image proposée sur la MarketPlace Azure
- la préparer pour servir d'image de base par la suite
- se servir du disque dur de cette VM pour créer une image de base custom
- créer une nouvelle VM ou plusieurs
  - à partir de notre image custom
  - elle(s) contiendra(ont) par défaut toute la conf qu'on avait préparé

> On peut dire que notre première VM va servir de *template* pour les autres.

# I. Création image custom

## 1. Créer une VM

🌞 **Créez une VM**

- uniquement avec la commande `az`
- OS de votre choix
  - le Ubuntu proposé par défaut par Azure c'est très bien
  - on sait que c'est stable, pour notre travail ici c'est cool
- conf ajoutée à la création :
  - un user
  - une clé publique pour s'y connecter

## 2. Configurer la VM

Ici on va ajouter de la configuration, peu importe finalement, c'est juste pour avoir notre ptite image custom.

On va en profiter pour faire des trucs utiles.

Je vous propose de faire deux trucs dans la VM :

- affiner la conf du serveur SSH pour qu'il soit + secure
- installer Docker, pour que la machine soit prête à lancer des conteneurs si besoin

➜ **Connectez-vous en SSH à la VM** pour effectuer les configurations qui vont suivre

🌞 **Installer docker à la machine**

- suivez les instructions de la doc officielle
- assurez-vous que le service démarre automatiquement lorsque la machine démarre (avec une commande `systemctl`)

🌞 **Améliorer la configuration du serveur SSH**

- on pense surtout à la sécurité ici
- je vous laisse trouver un bon guide sur internet, hésitez pas à m'appeler pour me demander mon avis !

> L'idée est de désactiver quelques features qu'on veut pas pour la sécu, et de forcer l'utilisation de chiffrements forts. Ni vous ni moi n'avez le savoir pour attester quels sont les chiffrements forts en 2024 : apprenez à consulter des ressources de confiance pour ces sujets là.

🌞 **Une fois que tout est fait : généraliser la VM**

- ce qu'on appelle "*généraliser*" c'est le fait de la remettre dans un état comme si elle n'avait jamais été lancée
- comme ça, elle pourra servir de base à la création d'autres VMs (c'est le but !)
- les étapes :
  - reset la conf réseau
  - on reset `cloud-init` pour qu'il se relance au prochain boot
  - on reset l'agent Azure (`wagent`) pour qu'il se lance au prochaine boot comme un premier boot
  - on supprime l'historique de commandes

```bash
# Suppression des confs réseau qui auraient pu être créées au premier boot
sudo rm -f /etc/cloud/cloud.cfg.d/50-curtin-networking.cfg /etc/cloud/cloud.cfg.d/curtin-preserve-sources.cfg /etc/cloud/cloud.cfg.d/99-installer.cfg /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg
sudo rm -f /etc/cloud/ds-identify.cfg
sudo rm -f /etc/netplan/*.yaml

# Conf déjà effectuée normalement mais au cas où : on demande à l'agent Azure d'utiliser cloud-init
sudo sed -i 's/Provisioning.Enabled=y/Provisioning.Enabled=n/g' /etc/waagent.conf
sudo sed -i 's/Provisioning.UseCloudInit=n/Provisioning.UseCloudInit=y/g' /etc/waagent.conf
sudo sed -i 's/ResourceDisk.Format=y/ResourceDisk.Format=n/g' /etc/waagent.conf
sudo sed -i 's/ResourceDisk.EnableSwap=y/ResourceDisk.EnableSwap=n/g' /etc/waagent.conf
cat <<EOF | sudo tee -a /etc/waagent.conf
Provisioning.Agent=auto
EOF

# On reset cloud-init
sudo cloud-init clean --logs --seed
sudo rm -rf /var/lib/cloud/

# On reset l'agent Azure
sudo systemctl stop walinuxagent.service
sudo rm -rf /var/lib/waagent/
sudo rm -f /var/log/waagent.log
sudo waagent -force -deprovision+user

# Suppression de l'historique de commande
sudo rm -f ~/.bash_history
```

➜ **VM prête !**

⚠️⚠️⚠️ **Éteignez la VM avant de continuer** (depuis la WebUI Azure ou avec `az`) **puis saisisse zles commandes suivantes :** ⚠️⚠️⚠️

```bash
# supprimer les ressources attribuées à la VM
az vm deallocate --resource-group <RESOURCE_GROUP> --name <VM_NAME> 

# transformer la VM en une VM "généralisée" (nécessaire pour en faire une image pour la suite)
az vm generalize --resource-group <RESOURCE_GROUP> --name <VM_NAME> 
```

## 3. Créer une image custom

![Azure demonos](./img/azure.jpg)

Pour ça il faut respecter le modèle défini par Azure, à savoir :

- créer une Galerie d'images
  - on y stockera notre image custom
- créer une Définition d'image
  - c'est genre dire qu'on a une image qui s'appelle "super-toto" par exemple
- créer une Version d'image
  - c'est indiquer qu'il existe une version "1.0.0" à "super-toto"
  - et on indique aussi que cette version utilise un disque dur précis
  - ce disque dur, ce sera celui de la VM créée précédemment

🌞 **Pour ça, suivez les commandes `az` suivantes :**

```bash
# Repérez le nom de votre VM précédemment créée
az vm list --output table

# Repérez l'ID de la VM, il est sous la forme :
# /subscriptions/<ID>/resourceGroups/<RESOURCE_GROUP>/providers/Microsoft.Compute/virtualMachines/<VM_NAME>
az vm get-instance-view -g <RESOURCE_GROUP> -n <VM_NAME>

# Créer une galerie d'image
az sig create \
   --resource-group efreitp2 \
   --gallery-name efreigallery \

# Créer une définition d'image
az sig image-definition create \
   --resource-group efreitp2 \
   --gallery-name efreigallery \
   --gallery-image-definition efreidef \
   --publisher efreipub \
   --offer efreioffer \
   --sku efreisku \
   --os-type Linux \
   --os-state Generalized \
   --hyper-v-generation V2 \
   --features SecurityType=TrustedLaunch

# Publier une version de l'image
# c'est là qu'on précise qu'on se sert de la VM d'avant comme base
# après --managed-image, remplacez par l'ID que vous avez récupéré plus tôt
az sig image-version create \
   --resource-group efreitp2 \
   --gallery-name efreigallery \
   --gallery-image-definition efreidef \
   --gallery-image-version 1.0.0 \
   --target-regions "francecentral" \
   --replica-count 1 \
   --managed-image "/subscriptions/<ID>/resourceGroups/<RESOURCE_GROUP>/providers/Microsoft.Compute/virtualMachines/<VM_NAME>"
```

➜ **Notez bien l'ID de l'image une fois que cette dernière commande est passée**

- dans le retour de la commande sera indiqué l'ID de l'image
- l'ID est sous la forme :

```weird
"/subscriptions/<ID>/resourceGroups/<RESOURCE_GROUP>/providers/Microsoft.Compute/galleries/<GALLERY>/images/<DEFINITION>/versions/1.0.0"
```

> *Toutes les opérations se déroule sur Azure, si ça prend pas tu temps, ça dépend pas de votre connexion actuelle sur votre PC.*

## 3. Créer une nouvelle VM

```bash
# on crée un nouvelle VM en indiquant qu'on veut utiliser notre nouvelle image comme base
# on crée un user et on dépose une clé publique à la création de la vm
az vm create \
    --resource-group efreitp2 \
    --name prout \
    --image "/subscriptions/<ID>/resourceGroups/<RESOURCE_GROUP>/providers/Microsoft.Compute/galleries/<GALLERY>/images/<DEFINITION>/versions/1.0.0" \
    --security-type TrustedLaunch \
    --ssh-key-values <CHEMIN_VERS_CLE_PUB> --admin-username <TON_USER>
```

- `<CHEMIN_VERS_CLE_PUB>` : le chemin vers ta clé publique sur ton PC
- `<TON_USER>` : nom du user que tu veux créer au lancement de la machine

> C'est le service `cloud-init` qui s'occupe de créer l'utilisateur et de déposer la clé publique au démarrage de la machine.

➜ **Connectez-vous en SSH à cette nouvelle VM** et constatez :

- Docker est déjà installé et démarré
- y'a bien votre configuration SSH
- on peut donc créer autant de VM qu'on veut à partir d'une image de base maîtrisée
- `cloud-init` a bien été run

# II. Tester : spécialiser les VMs au lancement

On va revenir sur `cloud-init`, cette fois, avec une image qui est déjà préconfigurée.

On a quelque chose d'assez puissant sous la main là, Docker est déjà installé, on peut donc avec quelques lignes de conf `cloud-init` démarrer des nouvelles VMs avec des conteneurs déjà actifs quand elles s'allument.

Bien qu'on ait fait ça de façon sommaire, on a un truc très proche de la réalité :

- une image de base customisée
- un outil pour spécialiser les VMs au moment où elles sont lancées : `cloud-init`

🌞 **Sur votre PC, écrivez un fichier `cloud-init.txt`**

- réutiliser le fichier que je vous ai fourni plus haut pour créer un user
- ajoutez à ce fichier ce qu'il faut pour que `cloud-init` lance un conteneur NGINX :
  - ainsi, dès que la VM pop, PAF ! Un serveur web déjà dispo dans un conteneur
  - une ligne très simple suffit, par exemple :

```bash
systemctl start docker # si c'est pas déjà activé au démarrage
docker run -d -p 80:80 nginx
```

🌞 **Avec une comande `az`, créez une nouvelle machine**

- elle doit utiliser notre image custom
- elle doit fournir votre fichier `cloud-init.txt` à la VM pour que le service `cloud-init` applique son contenu
- une fois qu'elle a démarré, vérifiez que le serveur web tourne

➜ Si vous voulez visiter le site web, il sera nécessaire d'ouvrir un port dans le firewall Azure

- dans la WebUI :
  - aller dans la liste des VMs, cliquer sur la VM conernée
  - Networking > Network Settings > Ajouter une règle Inbound
- [avec `az network nsg rule create`](https://learn.microsoft.com/en-us/cli/azure/network/nsg/rule?view=azure-cli-latest)

# Bilan et cleanup

🌞 **Supprimez toutes les ressources dans votre espace Azure**

- rien à rendre dans le TP mais je l'écris pour pas que vous l'oubliez !
- supprimez les VMs étou étou, pour pas consommer des crédits Azure pour rien
- allez dans votre Resource Group créé au début du TP, supprimez-tout, et supprimez le Resource Group

➜ **Une nouvelle image de VM custom**

- avec notre politique de sécu inclus (conf SSH, on pourrait aller bien plus loin)
- avec de quoi lancer des nouvelles applications dès le boot (Docker)
- cas typique d'entreprise où chaque VM doit contenir une conf maîtrisée

![Happy](./img/api.png)
