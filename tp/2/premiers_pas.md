# Partie 1 : Premiers pas Azure

On commence tranquillement avec cette première partie !

- [Partie 1 : Premiers pas Azure](#partie-1--premiers-pas-azure)
- [I. Premiers pas](#i-premiers-pas)
- [II. cloud-init](#ii-cloud-init)

# I. Premiers pas

🌞 **Créez une VM depuis la WebUI**

- et vérifiez que vous pouvez vous y connecter en SSH en utilisant une clé

🌞 **Créez une VM depuis le Azure CLI**

- en utilisant uniquement la commande `az` donc
- assurez-vous que dès sa création terminée, vous pouvez vous connecter en SSH en utilisant une IP publique
- vous devrez préciser :
  - quel utilisateur doit être créé à la création de la VM
  - le fichier de clé utilisé pour se connecter à cet utilisateur
  - comme ça, dès que la VM pop, on peut se co en SSH !
- je vous laisse faire vos recherches pour créer une VM avec la commande `az`

> *Encore une fois, je vous recommande d'utiliser `az interactive`.*

Par exemple, une commande simple pour faire ça (ça suppose qu'une clé publique SSH existe dans `../.ssh/id_rsa.pub`):

```bash
az vm create -g meo -n super_vm --image Ubuntu2204 --admin-username it4 --ssh-key-values ../.ssh/id_rsa.pub
```

➜ **Assurez-vous que vous pouvez vous connecter à la VM en SSH sur son IP publique.**

- une fois connecté, observez :
  - **la présence du service `walinuxagent`**
    - permet à Azure de monitorer et interagir avec la VM
  - **la présence du service `cloud-init`**
    - permet d'effectuer de la configuration automatiquement au premier lancement de la VM
    - c'est lui qui a créé votre utilisateur et déposé votre clé pour se co en SSH !
    - vous pouvez vérifier qu'il s'est bien déroulé avec la commande `cloud-init status`

> Pratique de pouvoir se connecter en utilisant une IP publique comme ça ! En revanche votre offre *Azure for Students* ne vous donne le droit d'utiliser que 3 IPs publiques. Pensez donc bien à supprimer les ressources au fur et à mesure du TP.

🌞 **Créez deux VMs depuis le Azure CLI**

- assurez-vous qu'elles ont une IP privée (avec `ip a`)
- elles peuvent se `ping` en utilisant cette IP privée
- deux VMs dans un LAN quoi !

> *N'hésitez pas à vous rendre sur la WebUI de Azure pour voir vos VMs créées.*

# II. cloud-init

![cloud-init](./img/cloudinit.jpg)

`cloud-init` que vous avez aperçu à la partie précédente est un outil qui permet de configurer une VM dès son premier boot.

C'est bien beau de pop une VM dans le "cloud", mais comment on dépose notre clé SSH ? On aimerait éviter de se co avec un password, et ce, dès la première connexion.

`cloud-init` a donc pour charge de configurer la VM **juste après son premier boot**.

Il peut par exemple :

- créer des users
  - définir des password
  - définir une conf `sudo`
  - poser une clé publique
- installer des paquets
- déposer des fichiers de conf
- démarrer des services

On va rester simple ici est on va utiliser l'image `Ubuntu2204` officielle, fournie par Azure : elle supporte `cloud-int` !

➜ **Sur votre PC, créez un fichier `cloud-init.txt` avec le contenu suivant :**

```yml
#cloud-config
users:
  - name: <TON_USER>
    primary_group: <TON_GROUPE>
    groups: sudo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    passwd: <HASH_PASSWORD>
    ssh_authorized_keys:
      - <TA_CLE_PUBLIQUE>
```

- `<TON_USER>` : le nom du user que tu veux créer dans la VM
- `<TON_GROUPE>` : le groupe primaire de ce user
- `<HASH_PASSWORD>` : le hash du password que tu veux définir pour ce user
  - au format de `/etc/shadow`
  - vous pouvez générer le hash du password de votre choix avec la commande `python` juste en dessous
- `<TA_CLE_PUBLIQUE>` : la clé publique utilisée pour se co en SSH sur ce user

```bash
python3 -c "from getpass import getpass; from crypt import *; \
     p=getpass(); print('\n'+crypt(p, METHOD_SHA512)) \
     if p==getpass('Please repeat: ') else print('\nFailed repeating.')"
```

Une fois rempli, le fichier ressemble donc à ça :

```yml
#cloud-config
users:
  - name: it4
    primary_group: it4
    groups: sudo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    passwd: $6$XURsDlAF30pG3D3W$8a9/8WMPYRpGJdCujP/WiLSiojYfObsxfLpYokoISUDO0LqkaNd6bp8tnqow29aNl1oXvgU4CNK.1q76WNfAs/
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NUE5AAAAIMO/JQ3AtA3k8iXJWlkdUKSHDh215OKyLR0vauzD7BgA
```

🌞 **Tester `cloud-init`**

- en créant une nouvelle VM et en lui passant ce fichier `cloud-init.txt` au démarrage
- pour ça, utilisez une commande `az vm create`
- utilisez l'option `--custom-data /path/to/cloud-init.txt`

🌞 **Vérifier que `cloud-init` a bien fonctionné**

- connectez-vous en SSH à la VM nouvellement créée
- vous devriez observer qu'un nouvel utilisateur a été créé, avec le bon password et la bonne clé
