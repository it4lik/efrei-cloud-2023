# TP2 : Déploiement cloud

Dans ce TP on va se concentrer sur la création et la gestion d'environnement virtuel en utilisant une offre Cloud.

Au menu :

- **création de VM dans Azure**
- **création d'une image customisée**
  - on pourra ensuite s'en servir pour créer de nouvelles VMs
  - ces nouvelles VMs contiendont la conf qu'on aura défini dans l'image de base
  - ainsi, dès qu'une VM est créée, elle est déjà conf !
  - et surtout, cette conf, elle est orientée sécu
- **jouer avec `cloud-init`**
  - c'est un p'tit service (un programme) qui se lance au démarrage
  - il applique une configuration donnée (format `.yml`)
  - dès qu'une VM pop, elle est configurée par `cloud-init`
  - il applique la conf qu'on lui donne, et il lance les commandes qu'on demande !
  - dès qu'une VM pop, elle peut donc tout de suite lancer des trucs

![Cloouuuuud](./img/to_the_cloud.jpg)

# 0. Setup

➜ **Il faudra activer l'offre *Azure for Students***

- grâce à votre compte EFREI
- en suivant les instructions [sur ce lien](https://azure.microsoft.com/fr-fr/free/students)
- vous allez bénéficier de 100$ offerts sur la plateforme Azure

➜ **Assurez-vous d'avoir une paire de clés SSH sur votre machine**

- car vous utiliserez SSH pour vous connecter aux VMs créés dans Azure
- lors de la création de la VM, Azure posera votre clé publique dans la VM nouvellement créée
- ainsi vous pourrez vous y connecter tout de suite sans mot de passe de façon sécurisée

➜ **Assurez-vous depuis la GUI de Azure**

- que vous avez bien 100$ dispos
- que vous pouvez créer des ptites VMs

➜ **Setup le Azure CLI**, sympa pour bosser avec Azure

- on peut l'utiliser pour faire les mêmes actions que depuis la GUI
- mais en utilisant le terminal et la commande `az`
- suivez [la doc officielle](https://learn.microsoft.com/fr-fr/cli/azure/install-azure-cli) pour l'installer :)
- **utilisez la commande `az login` une fois que c'est installé pour vous connecter avec votre compte EFREI**

> Je vous conseille d'utiliser la commande `az interactive` pour tout faire : elle vous assistera en temps réel pour taper les commandes `az`

🌞 **Une fois que c'est fait, pour vérifier la bonne installatation...**

- on va toute de suite créer un "Groupe de ressources" ou *"Resource Group"*
- utilisez la commande suivante pour créer votre premier *Resource Group*

```bash
az group create --name efreitp2 --location francecentral
```

## Suite du TP

Le reste du TP est découpé en deux parties :

- [**Partie 1** : Premiers pas Azure](./premiers_pas.md)
- [**Partie 2** : Créer et utiliser une image custom](./image_custom.md)

> Pour ce TP, j'ai utilisé pas mal de ressources en ligne. Notamment des pages de la doc officielle sur [la généralisation de VMs](https://learn.microsoft.com/en-us/azure/virtual-machines/linux/create-upload-ubuntu) et la [création d'images custom](https://learn.microsoft.com/en-us/azure/virtual-machine-scale-sets/tutorial-use-custom-image-cli). Aussi [cette petite issue](https://github.com/Azure/azure-cli/issues/24972) qui m'a sauvé la vie. Je vous invite à cliquer mais suivez le flow du TP, cette note est est juste à titre informatif !

![Basic](./img/basic_tasks.png)
