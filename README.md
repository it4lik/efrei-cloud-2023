# EFREI-Network-2-2023

Ici vous trouverez tous les supports de cours concernant le cours Virtualisation Réseau EFREI de l'année 2023-2024.

## [TP](./tp/README.md)

- [TP1 : Containers](./tp/1/README.md)
- [TP2 : Déploiement cloud](./tp/2/README.md)

![No Cloud](./img/fsfe_nocloud.png)
